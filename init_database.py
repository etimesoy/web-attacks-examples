from werkzeug.security import generate_password_hash

from accounts.models import User, Document
from app import db, app


def main():
    with app.app_context():
        db.create_all()
        db.session.add(User(username='Rustem', hashed_password=generate_password_hash('123456')))
        db.session.add(User(username='Artur', hashed_password=generate_password_hash('123456')))
        db.session.add(Document(owner_id=1, name="Секретный документ 1"))
        db.session.add(Document(owner_id=2, name="Секретный документ 1"))
        db.session.add(Document(owner_id=2, name="Секретный документ 1"))
        db.session.commit()


if __name__ == '__main__':
    main()
